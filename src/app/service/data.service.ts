import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private HttpClient:HttpClient) { }

  /* Rutas propietarios */
  getData(){
    return this.HttpClient.get('http://localhost:8000/api/propietario');
  }

  getDataById(id){
    return this.HttpClient.get('http://localhost:8000/api/propietario/' + id);
  }

  insertData(data){
    return this.HttpClient.post('http://localhost:8000/api/addPropietario', data);
  }

  updateData(id, data){
    return this.HttpClient.put('http://localhost:8000/api/updatePropietario/' + id, data);
  }

  deleteData(id){
    return this.HttpClient.delete('http://localhost:8000/api/deletePropietario/' + id);
  }
  /* Rutas propietarios */

  /* Rutas conductores */
  getDataConductores(){
    return this.HttpClient.get('http://localhost:8000/api/conductor');
  }

  getDataConductorById(id){
    return this.HttpClient.get('http://localhost:8000/api/conductor/' + id);
  }

  insertDataConductores(data){
    return this.HttpClient.post('http://localhost:8000/api/addConductor', data);
  }

  updateDataConductores(id, data){
    return this.HttpClient.put('http://localhost:8000/api/updateConductor/' + id, data);
  }

  deleteDataConductores(id){
    return this.HttpClient.delete('http://localhost:8000/api/deleteConductor/' + id);
  }
  /* Rutas conductores */

  /* Rutas Vehiculos */
  getDataVehiculos(){
    return this.HttpClient.get('http://localhost:8000/api/vehiculo');
  }

  getDataVehiculoById(id){
    return this.HttpClient.get('http://localhost:8000/api/vehiculo/' + id);
  }

  insertDataVehiculos(data){
    return this.HttpClient.post('http://localhost:8000/api/addVehiculo', data);
  }

  updateDataVehiculos(id, data){
    return this.HttpClient.put('http://localhost:8000/api/updateVehiculo/' + id, data);
  }

  deleteDataVehiculos(id){
    return this.HttpClient.delete('http://localhost:8000/api/deleteVehiculo/' + id);
  }
  /* Rutas Vehiculos */
}
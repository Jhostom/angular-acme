import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { PropietarioComponent } from './components/propietario/propietario.component';
import { NavbarComponent } from './components/navbar/navbar.component';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { PropietarioEditComponent } from './components/propietario/propietario-edit/propietario-edit.component';
import { ConductorComponent } from './components/conductor/conductor.component';
import { ConductorEditComponent } from './components/conductor/conductor-edit/conductor-edit.component';
import { VehiculoComponent } from './components/vehiculo/vehiculo.component';
import { VehiculoEditComponent } from './components/vehiculo/vehiculo-edit/vehiculo-edit.component';
import { HomeComponent } from './components/home/home.component';

const appRoutes: Routes = [
  
  {
    path: '', component:HomeComponent
  },
  {
    path: 'propietario', component:PropietarioComponent,
  },
  {
    path: 'propietario/edit/:id', component:PropietarioEditComponent,
  },
  {
    path: 'conductor', component:ConductorComponent
  },
  {
    path: 'conductor/edit/:id', component:ConductorEditComponent,
  },
  {
    path: 'vehiculo', component:VehiculoComponent
  },
  {
    path: 'vehiculo/edit/:id', component:VehiculoEditComponent,
  },
];

@NgModule({
  declarations: [
    AppComponent,
    PropietarioComponent,
    NavbarComponent,
    PropietarioEditComponent,
    ConductorComponent,
    ConductorEditComponent,
    VehiculoComponent,
    VehiculoEditComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

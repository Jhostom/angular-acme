import { Component, OnInit } from '@angular/core';
import { Vehiculo } from 'src/app/vehiculo';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-vehiculo',
  templateUrl: './vehiculo.component.html',
  styleUrls: ['./vehiculo.component.css']
})
export class VehiculoComponent implements OnInit {

  vehiculo: any;
  conductor: any;
  propietario: any;
  vehiculos = new Vehiculo();

  constructor(private dataService:DataService) { }

  ngOnInit(): void {
    this.getVehiculosData();
  }

  getVehiculosData(){
    this.dataService.getDataVehiculos().subscribe(res => {
      this.vehiculo = res;
    });
    this.dataService.getDataConductores().subscribe(res => {
      this.conductor = res;
    });
    this.dataService.getData().subscribe(res => {
      this.propietario = res;
    });
  }

  insertData(){
    this.dataService.insertDataVehiculos(this.vehiculos).subscribe(res => {
      this.getVehiculosData();
    });
  }

  deleteData(id){
    this.dataService.deleteDataVehiculos(id).subscribe(res => {
      this.getVehiculosData();
    });
  }
}

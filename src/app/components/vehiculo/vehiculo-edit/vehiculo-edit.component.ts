import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Vehiculo } from 'src/app/vehiculo';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-vehiculo-edit',
  templateUrl: './vehiculo-edit.component.html',
  styleUrls: ['./vehiculo-edit.component.css']
})
export class VehiculoEditComponent implements OnInit {

  id: any;
  data: any;
  conductor: any;
  propietario: any;
  vehiculos = new Vehiculo();

  constructor(private route:ActivatedRoute, private dataService: DataService, private router:Router,) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.getData();
  }

  getData(){
    this.dataService.getDataVehiculoById(this.id).subscribe( res => {
      this.data = res;
      this.vehiculos = this.data;
      console.log(this.vehiculos.color)
    });
    this.dataService.getDataConductores().subscribe(res => {
      this.conductor = res;
    });
    this.dataService.getData().subscribe(res => {
      this.propietario = res;
    });
  }

  updateVehiculo(){
    this.dataService.updateDataVehiculos(this.id, this.vehiculos).subscribe( res => {
      return this.router.navigate(['vehiculo']);
    });
  }

}

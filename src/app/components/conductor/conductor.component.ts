import { Component, OnInit } from '@angular/core';
import { Conductor } from 'src/app/conductor';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-conductor',
  templateUrl: './conductor.component.html',
  styleUrls: ['./conductor.component.css']
})
export class ConductorComponent implements OnInit {

  conductor: any;
  conductores = new Conductor();

  constructor(private dataService:DataService) {}

  ngOnInit(): void {
    type ValidationErrors = {
      [key: string]: any;
    };
    this.getConductoresData();
  }

  getConductoresData(){
    this.dataService.getDataConductores().subscribe(res => {
      this.conductor = res;
    });
  }

  insertData(){
    this.dataService.insertDataConductores(this.conductores).subscribe(res => {
      this.getConductoresData();
    });
  }

  deleteData(id){
    this.dataService.deleteDataConductores(id).subscribe(res => {
      this.getConductoresData();
    });
  }
}
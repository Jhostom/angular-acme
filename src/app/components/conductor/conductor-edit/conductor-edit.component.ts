import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Conductor } from 'src/app/conductor';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-conductor-edit',
  templateUrl: './conductor-edit.component.html',
  styleUrls: ['./conductor-edit.component.css']
})
export class ConductorEditComponent implements OnInit {

  id: any;
  data: any;
  conductores = new Conductor();

  constructor(private route:ActivatedRoute, private dataService: DataService, private router:Router,) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.getData();
  }

  getData(){
    this.dataService.getDataConductorById(this.id).subscribe( res => {
      this.data = res;
      this.conductores = this.data;
    });
  }

  updatePropietario(){
    this.dataService.updateDataConductores(this.id, this.conductores).subscribe( res => {
      return this.router.navigate(['conductor']);
    });
  }

}

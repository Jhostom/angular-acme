import { Component, OnInit } from '@angular/core';
import { Propietario } from 'src/app/propietario';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-propietario',
  templateUrl: './propietario.component.html',
  styleUrls: ['./propietario.component.css']
})
export class PropietarioComponent implements OnInit {

  propietario: any;
  propietarios = new Propietario();

  constructor(private dataService:DataService) { }

  ngOnInit(): void {
    this.getPropietariosData();
  }

  getPropietariosData(){
    this.dataService.getData().subscribe(res => {
      this.propietario = res;
    });
  }

  insertData(){
    this.dataService.insertData(this.propietarios).subscribe(res => {
      this.getPropietariosData();
    });
  }

  deleteData(id){
    this.dataService.deleteData(id).subscribe(res => {
      this.getPropietariosData();
    });
  }
}

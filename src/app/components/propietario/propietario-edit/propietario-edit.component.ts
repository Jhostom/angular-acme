import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Propietario } from 'src/app/propietario';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-propietario-edit',
  templateUrl: './propietario-edit.component.html',
  styleUrls: ['./propietario-edit.component.css']
})
export class PropietarioEditComponent implements OnInit {

  id: any;
  data: any;
  propietarios = new Propietario();

  constructor(private route:ActivatedRoute, private dataService: DataService, private router:Router,) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.getData();
  }

  getData(){
    this.dataService.getDataById(this.id).subscribe( res => {
      this.data = res;
      this.propietarios = this.data;
    });
  }

  updatePropietario(){
    this.dataService.updateData(this.id, this.propietarios).subscribe( res => {
      return this.router.navigate(['propietario']);
    });
  }
}
